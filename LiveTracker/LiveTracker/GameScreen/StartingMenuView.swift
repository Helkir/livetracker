//
//  StartingMenuView.swift
//  LiveTracker
//
//  Created by Arnaud Chrétien on 30/09/2020.
//  Copyright © 2020 Coconuterino. All rights reserved.
//

import SwiftUI

struct StartingMenuView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct StartingMenuView_Previews: PreviewProvider {
    static var previews: some View {
        StartingMenuView()
    }
}
