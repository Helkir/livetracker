//
//  MainMenuView.swift
//  LiveTracker
//
//  Created by Arnaud Chrétien on 30/09/2020.
//  Copyright © 2020 Coconuterino. All rights reserved.
//

import SwiftUI

struct MainMenuView: View {
    @State var players: [Player] = Player.makeDummyList()
    var body: some View {
        ZStack{
            NavigationView {
                VStack(alignment: .center, spacing: 0, content: {
                    HStack {
                        NavigationLink(destination: PlayerView(player: players[3])) {
                            PlayerCompactView(player: players[3]).offset(x: 0, y: 32)
                        }
                    }
                    HStack {
                        PlayerCompactView(player: players[2])
                        Spacer()
                        ButtonView(players: players, action: .hands).offset(x: 0, y: 10)
                        Spacer()
                        PlayerCompactView(player: players[4])
                    }
                    HStack {
                        PlayerCompactView(player: players[1])
                        Spacer()

                        PlayerCompactView(player: players[5])
                    }
                    HStack {
                        PlayerCompactView(player: players[0]).offset(x: 0, y: -32)
                    }
                }).zIndex(1)
                .navigationBarHidden(true)
            }
        }
    }
}

struct MainMenuView_Previews: PreviewProvider {
    static var previews: some View {
        MainMenuView().previewLayout(.fixed(width: 600, height: 380))
    }
}
