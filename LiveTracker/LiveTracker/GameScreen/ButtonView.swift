import Foundation
import SwiftUI

struct ButtonView: View {
    var players : [Player]
    var action:Statistique
    var color = Color.green
    var body: some View {
        Button(action: {
            for player in players {
                player.addStat(stat: action)
                print(player.stats.hands)
            }
        }) {
            Text("NEW HAND")
                .fontWeight(.bold).font(.subheadline)
                .padding()
                .background(Color.green)
                .cornerRadius(40)
                .foregroundColor(.white)
                .padding(10)
                .overlay(
                    RoundedRectangle(cornerRadius: 40)
                        .stroke(color, lineWidth: 5)
                )
        }.frame(width: 180, height: 60, alignment: .center).offset(x: 0, y: 50)
    }
}

struct ButtonView_Previews: PreviewProvider {
    static var previews: some View {
        ButtonView(players: Player.makeDummyList(), action: .hands)
    }
}
