import Foundation

struct Stats {
    var hands: Int // +1/hand
    var vpip: Int //+1 si bet
    var pfr: Int //+1 si raise pf
    var bet3: Int //+1 si 3bet
    var bet3Fold : Int // +1 si 3bf
    var stl: Int //+1 si stl
    var foldToSteal: Int //+1 si ftstl
    var cbet: Int // +1 si cbet
    var foldToCbet: Int //+1 si fcbet
    
    static func makeDummy() -> Stats {
        return Stats(hands: 543, vpip: 28, pfr: 22, bet3: 6, bet3Fold: 60, stl: 5, foldToSteal: 55, cbet: 70, foldToCbet: 40)
    }

}

public enum Statistique: Int {
    case hands // +1/hand
    case vpip //+1 si bet
    case pfr //+1 si raise pf
    case bet3 //+1 si 3bet
    case bet3Fold // +1 si 3bf
    case stl//+1 si stl
    case foldToSteal//+1 si ftstl
    case cbet// +1 si cbet
    case foldToCbet //+1 si fcbet
}
