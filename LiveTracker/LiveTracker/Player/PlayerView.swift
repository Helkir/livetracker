//
//  PlayerView.swift
//  LiveTracker
//
//  Created by Arnaud Chrétien on 28/08/2020.
//  Copyright © 2020 Coconuterino. All rights reserved.
//

import Foundation
import SwiftUI

struct PlayerView: View {
    var player: Player
    var body: some View {
        VStack {
            HStack {
                Text(player.name).bold()
                Spacer()
                Text("HANDS:")
                Text(String(player.stats.vpip)).bold()
            }
            HStack {
                Text("VPIP:")
                Spacer()
                Text(String(player.stats.vpip)).bold()
            }
            HStack {
                Text("PFR:")
                Spacer()
                Text(String(player.stats.vpip)).bold()
            }
            HStack {
                Text("3BET:")
                Spacer()
                Text(String(player.stats.vpip)).bold()
            }
            HStack {
                Text("F@3BET:")
                Spacer()
                Text(String(player.stats.vpip)).bold()
            }
            HStack {
                Text("CBET:")
                Spacer()
                Text(String(player.stats.vpip)).bold()
            }
            HStack {
                Text("F@CBET:")
                Spacer()
                Text(String(player.stats.vpip)).bold()
            }
            HStack {
                Text("STL:")
                Spacer()
                Text(String(player.stats.vpip)).bold()
            }
            HStack {
                Text("F@STL:")
                Spacer()
                Text(String(player.stats.vpip)).bold()
            }
            BottomBarView()
        }.padding(16)

    }
    
}

struct PlayerView_Previews: PreviewProvider {
    static var previews: some View {
        PlayerView(player: Player(name: "Nono",id: 1)).previewLayout(.fixed(width: 600, height: 380))
    }
}
