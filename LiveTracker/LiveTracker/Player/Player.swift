//
//  Player.swift
//  LiveTracker
//
//  Created by Arnaud Chrétien on 28/08/2020.
//  Copyright © 2020 Coconuterino. All rights reserved.
//

import Foundation

class Player: Identifiable {
    var id: Int
    var name: String
    var stats: Stats
    
    init(name: String, id: Int) {
        self.id = id
        self.name = name
        self.stats = Stats.init(hands: 0, vpip: 0, pfr: 0, bet3: 0, bet3Fold: 0, stl: 0, foldToSteal: 0, cbet: 0, foldToCbet: 0)
    }
    
    func addStat(stat: Statistique) {
        
        switch stat {
        
        case .hands:
            self.stats.hands += 1
        case .vpip:
            self.stats.vpip += 1
        case .pfr:
            self.stats.pfr += 1
        case .bet3:
            self.stats.bet3 += 1
        case .bet3Fold:
            self.stats.bet3Fold += 1
        case .stl:
            self.stats.stl += 1
        case .foldToSteal:
            self.stats.foldToSteal += 1
        case .cbet:
            self.stats.cbet += 1
        case .foldToCbet:
            self.stats.foldToCbet += 1
        }
    }
    
    static func makeDummy() -> Player {
        return Player(name: "John", id: 1)
    }
    
    static func makeDummyList() -> [Player] {
        return [Player(name: "John", id: 1), Player(name: "Roger", id: 2) , Player(name: "Alex", id: 3) ,
                Player(name: "Yo", id: 4), Player(name: "Vanessa", id: 5) ,Player(name: "Chris", id: 6)]
    }
}
