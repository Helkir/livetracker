//
//  PlayerCompactView.swift
//  LiveTracker
//
//  Created by Arnaud Chrétien on 29/09/2020.
//  Copyright © 2020 Coconuterino. All rights reserved.
//

import SwiftUI

struct PlayerCompactView: View {
    @State var player: Player
    var body: some View {
        VStack {
            Text("\(player.name)"+" : \(player.stats.hands)").bold().font(.system(size: 18))
            Text("VPIP : \(player.stats.vpip)")
            Text("PFR : \(player.stats.pfr)")
            Text("3bet : \(player.stats.bet3)")
        }.padding().frame(width: 150, height: 110, alignment: .center).border(Color.blue)
    }
}

struct PlayerCompactView_Previews: PreviewProvider {
    static var previews: some View {
        PlayerCompactView(player: Player.makeDummy())
    }
}
