//
//  PlayerView.swift
//  LiveTracker
//
//  Created by Arnaud Chrétien on 28/08/2020.
//  Copyright © 2020 Coconuterino. All rights reserved.
//

import Foundation
import SwiftUI

struct PlayerBarView: View {
    var players: [Player]
    var body: some View {
        HStack {
            ForEach(players) { player in
                PlayerCompactView(player: player).frame()
            }   
        }
    }
}
    
    struct PlayerBarView_Previews: PreviewProvider {
        static var previews: some View {
            PlayerBarView(players: Player.makeDummyList()).previewLayout(.fixed(width: 568, height: 320))
        }
    }
