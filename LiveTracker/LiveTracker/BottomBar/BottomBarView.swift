//
//  BottomBarView.swift
//  LiveTracker
//
//  Created by Arnaud Chrétien on 28/08/2020.
//  Copyright © 2020 Coconuterino. All rights reserved.
//

import SwiftUI

struct BottomBarView: View {
    var body: some View {
        VStack(alignment: .center, spacing: nil ,content: {
            HStack(alignment: .bottom , spacing: 1, content: {
                BoxView(actionName: "Raised")
                BoxView(actionName: "Called")
                BoxView(actionName: "3et")
                BoxView(actionName: "STL")
                BoxView(actionName: "3BetFold")
                BoxView(actionName: "Cbet")
                BoxView(actionName: "CbetFold")
            }).padding(6)
        }).padding(16)
    }
}

struct BottomBarView_Previews: PreviewProvider {
    static var previews: some View {
        BottomBarView().previewLayout(.fixed(width: 568, height: 320))
    }
}

private struct BoxView: View {
    
    let actionName:String
    var body: some View {
        Button(action: {
            print(self.actionName)
        }) {
            Text(actionName)
                .fontWeight(.bold)
                .font(.callout)
                .padding()
                .background(Color.blue)
                .foregroundColor(.white)
                .padding(4)
                .frame(width: 80, height: 75, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
        }
    }
    
}
